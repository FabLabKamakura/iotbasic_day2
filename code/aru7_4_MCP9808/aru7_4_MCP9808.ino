// MCP9808.ino
#include <Wire.h>
#include <Adafruit_MCP9808.h>
Adafruit_MCP9808 mcp = Adafruit_MCP9808();

void setup() {
  Serial.begin(9600);
  Serial.println("MCP9808 Demo");
}
void loop() {
  if ( !mcp.begin()) {
    Serial.println("Couldnt find MCP9808");
    return;
  }  
  float c = mcp.readTempC();
  float f = c * 9.0 / 5.0 + 32;
  Serial.print("Temp: "); Serial.print(c); Serial.print("*C\t");
  Serial.print(f); Serial.println("*F");
  delay(250);

  Serial.println("Shutdown MCP9808…");
  mcp.shutdown_wake(1);
  delay(2000);
  Serial.println("wake up MCP9808…");
  mcp.shutdown_wake(0);
}
